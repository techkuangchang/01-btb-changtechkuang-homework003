package com.kshrd.homework003_changtechkuang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent(MainActivity.this,AnimationActivity.class);
        switch (item.getItemId()){
            case R.id.menu_add:
                return true;
            case R.id.fade_in:
                intent.putExtra("fadeIn","1");
                startActivity(intent);
                return true;
            case R.id.fade_out:
                intent.putExtra("fadeOut","2");
                startActivity(intent);
                return true;
            case R.id.zoom:
                intent.putExtra("zoom","3");
                startActivity(intent);
                return true;
            case R.id.rotate:
                intent.putExtra("rotate","4");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}