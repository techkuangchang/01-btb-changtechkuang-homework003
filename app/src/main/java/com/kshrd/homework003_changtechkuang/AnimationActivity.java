package com.kshrd.homework003_changtechkuang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class AnimationActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        String st = "1";
        String st2 = "2";
        String st3 = "3";
        String st4 = "4";

        String fadeIn = getIntent().getStringExtra("fadeIn");
        String fadeOut = getIntent().getStringExtra("fadeOut");
        String zoom = getIntent().getStringExtra("zoom");
        String rotate = getIntent().getStringExtra("rotate");

        if(st.equals(fadeIn)){
            imageView = findViewById(R.id.imageFan);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
            imageView.startAnimation(animation);
        }
        else if(st2.equals(fadeOut)){
            imageView = findViewById(R.id.imageFan);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_out);
            imageView.startAnimation(animation);
        }
        else if(st3.equals(zoom)){
            imageView = findViewById(R.id.imageFan);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoom);
            imageView.startAnimation(animation);
        }
        else if(st4.equals(rotate)){
            imageView = findViewById(R.id.imageFan);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
            imageView.startAnimation(animation);
        }


    }
}